function flatten(elements) {
    let newArray = [];
    if (typeof elements === 'array') {
        for (let i = 0; i < elements.length; i++) {
            if (typeof elements[i] != 'object') {
                newArray.push(elements[i]);
            } else {
                let item = flatten(elements[i])
                newArray = newArray.concat(item);
            }
        }
        return newArray;
    } else {
        console.log("Invalid input type");
    }
}

module.exports = flatten;
