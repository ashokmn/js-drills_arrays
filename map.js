function map(elements, cb) {
    let newArray = [];
    if (typeof elements === 'array') {
        for (let i = 0; i < elements.length; i++) {
            let newNumber = cb(elements[i])
            newArray.push(newNumber);
        }
        return newArray;
    } else {
        console.log("Invalid input type");
    }
}

module.exports = map;
