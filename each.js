function each(elements, cb) {
    if (typeof elements === 'array') {
        for (let i = 0; i < elements.length; i++) {
            return cb(elements[i], i);
        }
    } else {
        console.log("Invalid input type");
    }
}

module.exports = each;
