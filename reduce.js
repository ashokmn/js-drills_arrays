function reduce(elements, cb, startingValue) {
    let index = 0;
    if (typeof elements === 'array') {
        if (startingValue === undefined) {
            startingValue = elements[0];
            index = 1;
        }
        for (let i = index; i < elements.length; i++) {
            let reducer = cb(startingValue, elements[i]);
            startingValue = reducer;
        }
        return startingValue;
    } else {
        console.log("Invalid input type");
    }
}

module.exports = reduce;
