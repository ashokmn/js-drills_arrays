const filter = (elements, cb) => {
    let newArray = [];
    if (typeof elements === 'array') {
        for (let i = 0; i < elements.length; i++) {
            if (cb(elements[i])) {
                newArray.push(elements[i]);
            }
        }
        return newArray;
    } else {
        console.log("Invalid input type");
    }
}

module.exports = filter;
