const test = require('../find');

function checkFor(number){
    return number === 4;
}

const items = [1, 2, 3, 4, 5, 5];

let expected = 4;
let actual = test(items,checkFor);

if (JSON.stringify(actual) == JSON.stringify(expected)) {
    console.log("Test Passed");
} else {
    console.log("Test Failed");
}
