const test = require('../each');

function print(number, index) {
    console.log(`items : ${number}, Index : ${index}`);
}

const items = [1, 2, 3, 4, 5, 5];

const actual = test(items, print);
const expected = undefined;

if (actual === expected) {
    console.log("Test Passed");
} else {
    console.log("Test Failed");
}
