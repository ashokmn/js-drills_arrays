const test = require('../filter')

function isEven(number) {
    return number % 2 === 0;
}

const items = [1, 2, 3, 4, 5, 5];

let expected = [2, 4];
let actual = test(items, isEven)

if (JSON.stringify(actual) == JSON.stringify(expected)) {
    console.log("Test Passed");
} else {
    console.log("Test Failed");
}
