const test = require('../map');

function multiplesOfTen(number) {
    return number * 10;
}

const items = [1, 2, 3, 4, 5, 5];

let expected = [10, 20, 30, 40, 50, 50];
let actual = test(items, multiplesOfTen);

if (JSON.stringify(actual) == JSON.stringify(expected)) {
    console.log("Test Passed");
} else {
    console.log("Test Failed");
}
