const test = require('../reduce')

const sum = (accumulator, value) => {
    return accumulator + value
}

const items = [1, 2, 3, 4, 5, 5];

let expected = 23;
let actual = test(items, sum, 3)

if (JSON.stringify(actual) == JSON.stringify(expected)) {
    console.log("Test Passed");
} else {
    console.log("Test Failed");
}
