const test = require('../flatten');

const items = [1, [2], [[3]], [[[4]]]]

const expected = [1, 2, 3, 4]
const actual = test(items);


if (JSON.stringify(actual) == JSON.stringify(expected)) {
    console.log("Test Passed");
} else {
    console.log("Test Failed");
}
