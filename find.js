function find(elements, cb) {
    if (typeof elements === 'array') {
        for (let i = 0; i < elements.length; i++) {
            if (cb(elements[i])) {
                return elements[i];
            }
        }
    } else {
        console.log("Invalid input type");
    }
}

module.exports = find;
